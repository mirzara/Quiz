/* 
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$("#register_button").click(registerMe);

function registerMe()
{
    $.ajax({
        method: "POST",
        url: "register?" + $("#register").serialize(),
        success: function (response)
        {
            if (response === "S")
            {
                $("#response.alert-danger").attr("class","alert alert-success");
                $("#response").show().text("Registration successfull. You can now login");
                $("#register input").val(null);
            }
            else
            {
                showRegErrorMsg(response);
            }
        }
    });
}
function showRegErrorMsg(response)
{
    $("#response").show().text(errormsgs(response));
}

function errormsgs(response)
{
    var msg="";
    if(response==="E1")
    {
        msg="Please fill in all required fields";
    }
    else if(response==="E2")
    {
        msg="Not a valid email";
    }
    else if(response==="E3")
    {
        msg="Email is already registered";
    }
    else if(response==="E4")
    {
        msg="Password cant be smaller then 6 characters";
    }
    else if(response==="E5")
    {
        msg="Passwords dont match";
    }
    else if(response==="E6"|| response==="E7")
    {
        var txt="delete";
        if(response==="E7")
            txt="edit";
        
        msg="Cant "+txt+" quiz that dont belong to you!";
    }
    else if(response==="E8")
    {
        msg="Cant delete or modify content that doesnt belong to you!";
    }
    else if(response==="E9")
    {
        msg="No such quiz, question or result!";
    }
    return msg;
}