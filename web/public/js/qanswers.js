/* 
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$(".EditQuestionButton").click(editQ);
$(".DeleteQuestionButton").click(editQ);

function editQ() {
    var id = $(event.target).attr("data-id");
    var action = $(event.target).attr("data-act");

    $.ajax({
        method: "POST",
        url: "editQA?" + "action=" + action + "&" + $(".saveQuestionAnswerForm#" + id).serialize(),
        success: function (response)
        {
            if (response == "S")
            {
                DisplayMsg(id, response, "Question edited");
            }
            else if (response == "D") {
                var parent = $("#" + id).parents();
                $(parent[0]).find("input,button").attr("disabled", true);
                DisplayMsg(id, "S", "Question deleted");
            }
            else
            {
                DisplayMsg(id, "E", errormsgs(response));
            }
        }
    });
}

