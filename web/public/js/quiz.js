/* 
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


$("#CreateQuizButton").click(createQuiz);

function createQuiz(event)
{
    var action = $(event.target).attr("data-act");
    var id = $(event.target).attr("data-id");
    var actionUrl = "&action=" + action + "&";
    var createB = $(event.target)

    createB.attr("disabled", true);

    $.ajax({
        method: "POST",
        url: "createQuiz?id=" + id + actionUrl + $("#createQuizForm").serialize(),
        success: function (response)
        {
            if (response.success)
            {
                QuizSaveMsg(action, response);
            }
            else
            {
                DisplayMsg("CreateQuiz", "E", errormsgs(response.message));
                createB.attr("disabled", false);
            }
        }
    });
}

function QuizSaveMsg(action, response) {
    if (action === "add")
    {
        DisplayMsg("CreateQuiz", "S", "Quiz created");
        setTimeout(function () {
            window.location.href = "createQuiz?id=" + response.id;
            $("#CreateQuizButton").attr("disabled", false);
        }, 500);
    }
    else if (action === "edit")
    {
        DisplayMsg("CreateQuiz", "S", "Quiz saved");
    }
}

$("#DeleteQuizButton").click(DeleteQuiz);

function DeleteQuiz(event)
{
    var delB=$(event.target);
    var id = delB.attr("data-id");
    delB.attr("disbled",true);
    
    $.ajax({
        method: "POST",
        url: "deleteQuiz?id=" + id,
        success: function (response)
        {
            if (response === "S")
            {
                DisplayMsg("DeleteQuiz", response, "Quiz deleted");
                setTimeout(function () {
                    $("#DeleteQuiz .response").css("display", "");
                    window.location.href = "Home";
                    delB.attr("disabled",false);
                }, 800)
            }
            else
            {
                DisplayMsg("DeleteQuiz", "E", errormsgs(response));
                delB.attr("disabled",false);
            }
        }
    });
}

$(".delete").click(function () {
    var id = $(this).attr("data-id");
    $("#DeleteQuizButton").attr("data-id", id)
});


//Add question and answers to db

$("#SaveQuestionButton").click(SaveQuestion);

function SaveQuestion()
{
    var questionB=$(event.target);
    questionB.attr("disabled",true);
    
    $.ajax({
        method: "POST",
        url: "saveQuestionAnswer?" + $("#saveQuestionAnswerForm").serialize(),
        success: function (response)
        {
            if (response === "S")
            {
                DisplayMsg("saveQuestionAnswerForm", response, "Question added");
                $("#saveQuestionAnswerForm input[type^=text]").val(null);
                $("input[name=correct]:checked").attr("checked", false);

                setTimeout(function () {
                    $("#saveQuestionAnswerForm .response").css("display", "");
                }, 2000)
            }
            else
            {
                DisplayMsg("saveQuestionAnswerForm", "E", errormsgs(response));
            }
            questionB.attr("disabled",false);
        }
    });
}

$("#FinishQuizButton,.FinishQuizButton").click(changePage);
function changePage() {
    window.location.href = "Home";
}

//Start quiz functions
$("#StartQuizButton").click({param: "1"}, startQ);

function startQ(param) {
    var param = param.data.param;
    var url = "";

    if (param === "1") {

        var elem = $("#RunQuiz");
        elem.remove();

    } else if (param === "2") {

        url = "?correct=" + $("input[name=correct]:checked").val();

    }

    $.ajax({
        method: "POST",
        url: "runQuiz" + url,
        contentType: "application/json",
        success: function (response)
        {
            if (response.end) {
                $(".start").remove();
                showResults(response);
            }
            else {
                $(".start").removeClass("hide");
                showQuestionAnswers(response);
                showScore(response.correct, response.total);
            }
        }
    });
}
function showQuestionAnswers(obj) {
    var questionTxt = obj.qNo + ". " + obj.question;
    $("#questionNo").text(obj.qNo);
    $("#Answers #question").text(questionTxt);

    for (var i = 0; i < obj.answers.length; i++) {
        $(".ansId:eq(" + i + ")").val(obj.answers[i].id);
        $("#Answers #answer" + (i + 1)).text(obj.answers[i].answer);
    }

    $("#Answers").css("opacity", "0").animate({opacity: "1"}, 500);
}

function showScore(correctNo, total) {
    $("#correctNo").text(correctNo + "/" + total);
    $("#correctNo").hide().fadeIn(500);
}

function showResults(obj) {
    var percent = (obj.correct / obj.total) * 100;

    $("#results").show();

    $("#correctAns").text(obj.correct);
    $("#totalNo").text(obj.total);
    $("#percent").text(percent.toFixed(2) + "%");

    insertAndShow(obj.results);
}

//check answer and get a new one
$("#CheckQuestionButton").click({param: "2"}, startQ);

//Start quiz from beggining
$("#ReStartQuizButton").click(function () {
    location.reload();
})

//Change title if diffrent tab is selected

$('.nav-tabs a').on('shown.bs.tab', function () {
    var txt = "Add question and answers";

    var hrefVal = $(this).attr("href");

    if (hrefVal === "#resultTab")
        txt = "Add results";

    $("#tabTitle").text(txt);
});

function insertAndShow(results) {
    var html = [];

    $("<div id='resList' class='col-sm-6 col-sm-offset-3'></div>").insertBefore("footer")
    $("#resList").hide();

    for (var i = 0; i < results.length; i++) {

        html.push("<div class='col-sm-4 well'>");

        html.push(form_div("<b>" + results[i].title + "</b>"));
        html.push(form_div(results[i].from_p + "-" + results[i].till_p));
        html.push(form_div(results[i].description));

        html.push("</div>");
    }
    $("#resList").append(html.join(""));
    $("#resList").fadeIn(1500);
}

function form_div(add_Res) {
    var div = "<div class='col-sm-12'>";
    div = div + add_Res;
    div = div + "</div>";

    return div;
}

//Preview answers at the end of running quiz
$("#PreviewAnsButton").click(function () {
    window.open("previewAnswers");
})