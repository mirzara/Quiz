/* 
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$(".SaveResultButton").click(saveResults);
$(".DeleteResultButton").click(delResults);

function saveResults() {
    var saveB=$(event.target);
    var url = ".resultForm";
    var id = saveB.attr("data-id");
    var action = $(event.target).attr("data-act");
    saveB.attr("disabled",true);
    
    id=(id===undefined)?"":id;
    
    if (action === "edit")
        url = url + "#res_" + id;

    $.ajax({
        method: "POST",
        url: "saveResult?id=" + id + "&action=" + action + "&" + $(url).serialize(),
        success: function (response)
        {
            if (response === "S") {

                if (action === "add") {
                    DisplayMsg("res_", "S", "Result is added");
                    $(".resultForm input[type^=text]").val(null);

                    setTimeout(function () {
                        $("#res_ .response").css("display", "");
                    }, 2000)
                }
                else if (action === "edit") {
                    DisplayMsg("res_" + id, "S", "Result edited");
                }
            }
            else {
                DisplayMsg("res_" + id, "E", errormsgs(response));
            }
             saveB.attr("disabled",false);
        }
    });
}

function delResults() {
    var id = $(event.target).attr("data-id");

    $.ajax({
        method: "POST",
        url: "delResult?id=" + id,
        success: function (response)
        {
            if (response === "S")
            {
                var parent = $(".resultForm#res_" + id).parents();
                $(parent[0]).find("input,button").attr("disabled", true);
                DisplayMsg("res_" + id, "S", "Result deleted");
            }
        }
    });
}
//Limit to only numbers

$("#from_p,#till_p").keypress(NumbersOnly);
$("#from_p,#till_p").keyup(ifAbove100);

function NumbersOnly(event) {

    if (!(event.keyCode >= 48 && event.keyCode <= 57))
        return false;

    var value = $(this).val();

    if (value.length > 2)
        return false;

}


function ifAbove100() {

    var value = $(this).val();
    if (parseInt(value) > 100)
        $(this).val(100);

}
