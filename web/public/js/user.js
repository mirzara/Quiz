/* 
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$("#logoutButton").click(logoutNow);

function logoutNow()
{
    $.ajax({
        method: "POST",
        url: "logout",
        success: function (response)
        {
            if (response === "S")
                window.location.href = "login";
        }
    });
}

$("#DeleteAccButton").click(deleteAcc);

function deleteAcc()
{
    $.ajax({
        method: "POST",
        url: "deleteAcc",
        success: function (response)
        {
            if (response === "S")
            {
                DisplayMsg("deleteAcc",response,"Account deleted");
                setTimeout(function () {
                    window.location.href = "login";
                }
                , 3000)

            }
            else
            {
                DisplayMsg("deleteAcc",response,"Cant delete account");
            }
        }
    });
}

function DisplayMsg(elem, tip, msg)
{
    var class_add="alert-success";
    
    if (elem == null || elem === "")
        return;

    if (tip == null || tip === "")
        return;

    $("#" +elem+ " .response .alert:eq(0)").removeClass("alert-danger alert-success");

    if (tip === "E")
    {
        class_add="alert-danger";
    }
    $("#" +elem+ " .response .alert:eq(0)").addClass(class_add);
    $("#" +elem+ " .response .alert:eq(0)").text(msg);
    $("#" +elem+ " .response").show();
}

$("#changePassButton").click(changePassword);

function changePassword()
{
    
    $.ajax({
        method: "POST",
        url: "changePass?"+$("#changePassForm").serialize(),
        success: function (response)
        {
            if (response === "S")
            {
                DisplayMsg("ChangePass",response,"Password changed");
                $("#changePassForm input").val(null);
                
                setTimeout(function(){
                    $("#ChangePass").modal("hide");
                },1000);
            }
            else
            {
                DisplayMsg("ChangePass","E",errormsgs(response))
            }
        }
    });
}

function viewProfile()
{
    
    $.ajax({
        method: "POST",
        url: "viewProfile",
        contentType:"application/json",
        success: function (response)
        {
            appendResultsToForm(response);
        }
    });
}

function appendResultsToForm(response)
{
    var keys=Object.keys(response);
    
    for(var i=0;i<keys.length;i++)
    {
        $("#viewProfile #"+keys[i]).val(response[keys[i]]);
    }
}

$("#SaveProfileButton").click(saveProfile);

function saveProfile(response)
{
    $.ajax({
        method: "POST",
        url: "saveProfile?"+$("#viewProfileForm").serialize(),
        success: function (response)
        {
            if (response === "S")
            {
                DisplayMsg("viewProfile","S","Changes saved. Reloading page");
                setTimeout(function(){window.location.reload()},4000);
            }
            else
            {
                DisplayMsg("viewProfile","E",errormsgs(response))
            }
        }
    });
}
$("#viewProfile").on("show.bs.modal",viewProfile)