/* 
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


$("#Sort").change(ReloadWithParam);
$("#searchButton").click(ReloadWithParam);
$("#search").keypress(EnterSearch);

function ReloadWithParam(){
    var url=getUrl();
    var sort=$("#Sort").val();
    var valSearch=$("#search").val();
    var cat=$("#kategorija .active>a");
    
    if(cat[0]!=undefined){
        url=url+cat.attr("href");
    }
    
    url=url+"&search="+valSearch;
    
    if(!(sort===""))
        url=url+"&sort="+sort;
    
    window.location.href=url;
}

function EnterSearch(event){

    if(event.keyCode===13)
        ReloadWithParam();
    
}

function getUrl(){
    var url=window.location.pathname;
    url=url.replace("/","");
    
    var split_url=url.split("/");
    
    if(split_url.length>1)
        return split_url[1];
    
    return "";
}