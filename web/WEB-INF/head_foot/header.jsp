<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/public/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/public/css/style.css">
        <title>Quiz</title>
    </head>
    <body>

        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <a class="navbar-brand" href="${pageContext.request.contextPath}">Quiz</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/Home">Home</a></li>
                        <li><a href="${pageContext.request.contextPath}/Quizzes">Quizzes</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <% if (session.getAttribute("user") == null) { %>
                        <li><a href="${pageContext.request.contextPath}/login">Login</a></li>
                        <li><a href="${pageContext.request.contextPath}/register">Register</a></li>
                            <% } else { %>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">${sessionScope.user_fullname}<span class="caret"></span></a>               
                            <ul class="dropdown-menu">
                                <li>
                                    <a data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewProfile" href="#viewProfile">
                                        View Profile
                                    </a>
                                </li>
                                <li>
                                    <a data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#ChangePass" href="#ChangePass">
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#logout" href="#logout">
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <% }%>
                    </ul>
                </div>
            </div>
        </nav>

        <% if (session.getAttribute("user") != null) { %>
        <%@include file="../UserModals/usermodals.jsp" %>
        <%}%>