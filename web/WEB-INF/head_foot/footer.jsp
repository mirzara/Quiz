<footer class="footer">
    <div class="container" align="center">
        <p class="text-muted">Copyright 2016 Quiz</p>
    </div>
</footer>
<script src="public/js/jquery-1.12.3.min.js"></script>
<script src="public/js/login.js"></script>
<script src="public/js/register.js"></script>
<script src="public/js/home.js"></script>
<script src="public/js/user.js"></script>
<script src="public/js/quiz.js"></script>
<script src="public/js/qanswers.js"></script>
<script src="public/js/results.js"></script>
<script src="public/js/modalAction.js"></script>
<script src="public/js/bootstrap.min.js"></script>
</body>
</html>