

<div class="container">
    <div class="page-header">
        <h1 align="center">Login page</h1>
    </div>

    <div class="col-sm-6 col-sm-offset-3" align="center">
        <form id="login" class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-md-2 control-label">Email</label>  
                <div class="col-md-8">
                    <input id="user_email" name="user_email" type="text" maxlength="50" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Password</label>  
                <div class="col-md-8">
                    <input id="user_pass" name="user_pass" type="password" maxlength="50" class="form-control input-md">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="alert alert-danger" id="response">Problem</div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="form-group overfl">
    <div class="col-sm-12" align="center">
        <button type="button" class="btn btn-default" id="login_button">Login</button>
    </div>
</div>
