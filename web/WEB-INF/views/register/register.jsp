<div class="container">
    <div class="page-header">
        <h1 align="center">Registration</h1>
    </div>

    <div class="col-sm-6 col-sm-offset-3" align="center">
        <form id="register" class="form-horizontal" role="form">
            
            <div class="form-group">
                <label class="col-md-2 control-label">Name*</label>  
                <div class="col-md-8">
                    <input id="fname" name="fname" type="text" maxlength="50" class="form-control input-md">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label">Surname*</label>  
                <div class="col-md-8">
                    <input id="lname" name="lname" type="text" maxlength="50" class="form-control input-md">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label">Phone</label>  
                <div class="col-md-8">
                    <input id="phone" name="phone" type="text" maxlength="30" class="form-control input-md">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label">Address</label>  
                <div class="col-md-8">
                    <input id="address" name="address" type="text" maxlength="50" class="form-control input-md">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label">Email*</label>  
                <div class="col-md-8">
                    <input id="email" name="email" type="text" maxlength="50" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Password*</label>  
                <div class="col-md-8">
                    <input id="pass" name="pass" type="password" maxlength="50" class="form-control input-md">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="alert alert-danger" id="response">Problem</div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="form-group overfl">
    <div class="col-sm-12" align="center">
        <button type="button" class="btn btn-default" id="register_button">Register</button>
    </div>
</div>