<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="page-header text-center">
    <h3>Welcome to Home page</3>
</div>

<div class="col-sm-12 margin_bottom">

    <div class="col-sm-2">
        <input id="search" class="form-control input-md"/>
    </div>

    <div class="col-sm-2">
        <select class="form-control" id="Sort">
            <option value>Sort by</option>
            <option value="1" <c:if test="${param.sort=='1'}">selected</c:if>>Name</option>
            <option value="2" <c:if test="${param.sort=='2'}">selected</c:if>>Description</option>
            </select>
        </div>
        <button class="btn btn-primary" type="button" id="searchButton">Search</button>
    <c:if test="${requestScope.show && sessionScope.user!=null}">
        <button data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#CreateQuiz" class="btn btn-primary" type="button">Create Quiz</button>
    </c:if>
</div>                

<div class="col-sm-2 well">

    <ul class="nav nav-pills nav-stacked" id="kategorija">
        <li class="container">Category</li>
        <li <c:if test="${param.category==''||param.category==null}">class="active"</c:if>>
                <a href="?category=">All</a>
            </li>

        <c:forEach var="cat" items="${category}">
            <li 
                <c:if test="${param.category==cat.id}">class="active"</c:if>>
                <a href="?category=${cat.id}">${cat.name}</a>
            </li>
        </c:forEach>

    </ul>
</div>

<div class="col-sm-10">
    <c:forEach var="quizy" items="${quiz}">
        <div class="col-sm-3 well add_margin">
            <div class="col-sm-12 breakword overfl fix_head">
                <b>${quizy.name_test}</b>
            </div>

            <div class="col-sm-12 fixed_height breakword overfl margin_bottom">
                ${quizy.description}
            </div>

            <div class="col-sm-12">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <c:if test="${requestScope.show && sessionScope.user!=null}">
                            <li><a href="createQuiz?id=${quizy.id}">Add</a></li>
                            <li><a href="editQuiz?id=${quizy.id}">Edit</a></li>
                            <li><a data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#DeleteQuiz" data-id="${quizy.id}" href="#" class="delete">Delete</a></li>
                            </c:if>
                        <li><a href="runQuiz?id=${quizy.id}">Run</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </c:forEach>
</div>

<c:if test="${requestScope.show && sessionScope.user!=null}">
    <!-- Modal Create quiz -->
    <div class="modal fade" id="CreateQuiz" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" align="center">Create Quiz</h3>
                </div>
                <div class="modal-body">

                    <form id="createQuizForm" class="form-horizontal" role="form">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Name of Quiz*</label>  
                            <div class="col-md-8">
                                <input id="quiz_name" name="quiz_name" type="text" maxlength="50" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Description*</label>  
                            <div class="col-md-8">
                                <textarea id="description" name="description" type="text" maxlength="255" class="form-control input-md"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Category*</label>  
                            <div class="col-md-8">
                                <select id="categ" name="categ" class="form-control input-md">
                                    <c:forEach var="cat" items="${category}">
                                        <option id="${cat.id}" name="${cat.id}" value="${cat.id}">${cat.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="form-group response">
                            <div class="col-sm-12">
                                <div class="alert alert-danger">

                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="CreateQuizButton" data-act="add">Create</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="DeleteQuiz" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-center">Delete quiz</h3>
                </div>
                <div class="modal-body overfl">
                    <p>Are you sure you want to delete this quiz?</p>
                    <div class="form-group response">
                        <div class="col-sm-12">
                            <div class="alert alert-danger">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="DeleteQuizButton">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</c:if>