
<div class="page-header text-center">
    <h3 id="tabTitle">Add question and answers</h3>
</div>
<div class="col-sm-3 text-center">
    <ul class="nav nav-tabs well nav-pills nav-stacked">
        <li class="active"><a data-toggle="tab" href="#questionTab">Questions</a></li>
        <li><a data-toggle="tab" href="#resultTab">Results</a></li>
    </ul>
</div>

<div class="tab-content">
    <div id="questionTab" class="tab-pane active">

        <div class="col-sm-6 text-center margin_bottom">
            <form id="saveQuestionAnswerForm" class="form-horizontal" role="form">
                <input id="quiz_id" name="quiz_id" class="hide" value="${id}"/>
                <div class="form-group">
                    <label class="col-md-3 control-label">Enter Question</label>  
                    <div class="col-md-8 col-md-offset-1">
                        <input id="question" name="question" type="text" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Enter Answer 1

                    </label>  
                    <div class="col-md-1"> <input type="radio" name="correct" value="0"></div>
                    <div class="col-md-8">
                        <input id="answer1" name="answer1" type="text" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Enter Answer 2</label>  
                    <div class="col-md-1"> <input type="radio" name="correct" value="1"></div>
                    <div class="col-md-8">
                        <input id="answer2" name="answer2" type="text" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Enter Answer 3</label>  
                    <div class="col-md-1"> <input type="radio" name="correct" value="2"></div>
                    <div class="col-md-8">
                        <input id="answer3" name="answer3" type="text" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Enter Answer 4</label>  
                    <div class="col-md-1"> <input type="radio" name="correct" value="3"></div>
                    <div class="col-md-8">
                        <input id="answer4" name="answer4" type="text" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group response">
                    <div class="col-sm-12">
                        <div class="alert alert-danger">

                        </div>
                    </div>
                </div>

            </form>
            <div class="pull-right">
                <button type="button" class="btn btn-primary" id="SaveQuestionButton">Save Question</button>
                <button type="button" class="btn btn-default" id="FinishQuizButton">Finish Quiz</button>
            </div>
        </div>
    </div>

    <div id="resultTab" class="tab-pane fade">
        <div class="col-sm-6 text-center margin_bottom">
            <form id="res_" class="form-horizontal resultForm" role="form">
                <input id="quiz_id" name="quiz_id" class="hide" value="${id}"/>
                <div class="form-group">
                    <label class="col-md-3 control-label">Enter Title</label>  
                    <div class="col-md-8 col-md-offset-1">
                        <input id="title" name="title" type="text" maxlength="50" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">From percentage</label>  
                    <div class="col-md-8 col-md-offset-1">
                        <input id="from_p" name="from_p" type="text" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Till percentage</label>  
                    <div class="col-md-8 col-md-offset-1">
                        <input id="till_p" name="till_p" type="text" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Description</label>  
                    <div class="col-md-8 col-md-offset-1">
                        <input id="description" name="description" type="text" maxlength="255" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group response">
                    <div class="col-sm-12">
                        <div class="alert alert-danger">

                        </div>
                    </div>
                </div>

            </form>
            <div class="pull-right">
                <button type="button" class="btn btn-primary SaveResultButton" data-act="add">Save Result</button>
                <button type="button" class="btn btn-default" id="FinishQuizButton">Finish Quiz</button>
            </div>
        </div>
    </div>
</div>