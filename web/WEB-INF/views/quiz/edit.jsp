<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="page-header text-center">
    <h3>Edit Quiz</h3>
</div>

<div class="col-sm-3">
    <ul class="well nav nav-tabs nav-pills nav-stacked">
        <li class="active"><a data-toggle="tab" href="#CreateQuiz">Quiz</a></li>
        <li><a data-toggle="tab" href="#questionTab">Questions</a></li>
        <li><a data-toggle="tab" href="#resultTab">Results</a></li>
    </ul>
</div>

<div class="col-sm-6 tab-content">
    <div id="CreateQuiz" class="col-sm-12 text-center well margin_bottom tab-pane active">
        <form id="createQuizForm" class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-md-3 control-label">Enter name</label>
                <div class="col-md-8 col-md-offset-1">
                    <textarea id="quiz_name" name="quiz_name" type="text" maxlength="50" class="form-control input-md">${quiz.name_test}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Enter description</label>
                <div class="col-md-8 col-md-offset-1">
                    <textarea id="description" name="description" type="text" maxlength="255" class="form-control input-md">${quiz.description}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Select category</label>
                <div class="col-md-8 col-md-offset-1">
                    <select id="categ" name="categ" type="text" class="form-control input-md">
                        <c:forEach var="cat" items="${category}">
                            <option id="${cat.id}" name="${cat.id}" value="${cat.id}" <c:if test="${cat.id==quiz.getCategory().id}">selected</c:if>>
                                ${cat.name}
                            </option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group response">
                <div class="col-sm-12">
                    <div class="alert alert-danger">

                    </div>
                </div>
            </div>
        </form>
        <div class="pull-right">
            <button type="button" class="btn btn-primary" id="CreateQuizButton" data-id="${quiz.id}" data-act="edit">Save Quiz</button>
        </div>
    </div>

    <div id="questionTab" class="tab-pane fade">
        <c:if test="${questions.size()==0}">
            <div class="col-sm-12 well">
                <h4 class="text-danger">No questions found</h4>
            </div>
        </c:if>
        
        <c:forEach var="q" items="${questions}" varStatus="qLoop">

            <div class="col-sm-12 well text-center margin_bottom">
                <div class="page-header">Question number: ${qLoop.index+1}</div>
                <form id="que-${q.id}" class="form-horizontal saveQuestionAnswerForm" role="form">
                    <input id="quiz_id" name="quiz_id" class="hide" value="${quiz.id}"/>
                    <input id="question_id" name="question_id" class="hide question_id" value="${q.id}"/>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Enter Question</label>  
                        <div class="col-md-8 col-md-offset-1">
                            <input id="question" name="question" type="text" maxlength="255" class="form-control input-md" value="${q.question}">
                        </div>
                    </div>
                    <c:forEach var="a" items="${q.answerList}" varStatus="ansstat">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Enter Answer ${ansstat.index+1}

                            </label>  
                            <div class="col-md-1">
                                <input type="radio" name="correct" value="${ansstat.index}" ${a.correct==true?'checked':''}></div>
                            <div class="col-md-8">
                                <input id="answer${ansstat.index+1}" name="answer${ansstat.index+1}" type="text" maxlength="50" class="form-control input-md" value="${a.answer}">
                            </div>
                        </div>

                    </c:forEach>

                    <div class="form-group response">
                        <div class="col-sm-12">
                            <div class="alert alert-danger">

                            </div>
                        </div>
                    </div>

                </form>
                <div class="pull-right">
                    <button type="button" class="btn btn-primary EditQuestionButton" data-id="que-${q.id}" data-act="edit">Save Question</button>
                    <button type="button" class="btn btn-danger DeleteQuestionButton" data-id="que-${q.id}" data-act="delete">Delete Question</button>
                    <button type="button" class="btn btn-default FinishQuizButton">Finish Quiz</button>
                </div>
            </div>

        </c:forEach>
    </div>

    <div id="resultTab" class="tab-pane fade">
        <c:if test="${result.size()==0}">
            <div class="col-sm-12 well">
                <h4 class="text-warning">No results found</h4>
            </div>
        </c:if>
            
        <c:forEach var="res" items="${result}" varStatus="rLoop">
            <div class="col-sm-12 well text-center">
                <div class="page-header">Result number: ${rLoop.index+1}</div>
                <form id="res_${res.id}" class="form-horizontal resultForm" role="form">
                    <input id="quiz_id" name="quiz_id" class="hide" value="${quiz.id}"/>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Enter Title</label>  
                        <div class="col-md-8 col-md-offset-1">
                            <input id="title" name="title" type="text" maxlength="50" class="form-control input-md" value="${res.title}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">From percentage</label>  
                        <div class="col-md-8 col-md-offset-1">
                            <input id="from_p" name="from_p" type="text" class="form-control input-md" value="${res.from_percentage}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Till percentage</label>  
                        <div class="col-md-8 col-md-offset-1">
                            <input id="till_p" name="till_p" type="text" class="form-control input-md" value="${res.till_percentage}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Description</label>  
                        <div class="col-md-8 col-md-offset-1">
                            <input id="description" name="description" type="text" maxlength="255" class="form-control input-md" value="${res.description}">
                        </div>
                    </div>

                    <div class="form-group response">
                        <div class="col-sm-12">
                            <div class="alert alert-danger">

                            </div>
                        </div>
                    </div>

                </form>
                <div class="pull-right">
                    <button type="button" class="btn btn-primary SaveResultButton" data-act="edit" data-id="${res.id}">Save Result</button>
                    <button type="button" class="btn btn-danger DeleteResultButton" data-id="${res.id}">Delete Result</button>
                    <button type="button" class="btn btn-default FinishQuizButton">Finish Quiz</button>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

<div class="col-sm-3">
    <ul class="well nav">
        <li><b>Quiz</b></li>
        <li>${quiz.name_test}</li>
        <li><b>Questions</b></li>
        <li>${questions.size()}</li>
        <li><b>Results</b></li>
        <li>${result.size()}</li>
    </ul>
</div>