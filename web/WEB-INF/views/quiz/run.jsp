<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="page-header text-center">
    <h1>Run quiz</h1>
</div>

<c:if test="${quiz!=null}">
    <div id="RunQuiz" class="col-sm-6 col-sm-offset-3 text-center margin_bottom">
        <div class="form-group margin_bottom overfl">
            <label class="col-md-3 control-label">Name</label>
            <div class="col-md-8 col-md-offset-1">
                <textarea type="text" class="form-control input-md">${quiz.name_test}</textarea>
            </div>
        </div>

        <div class="form-group margin_bottom overfl">
            <label class="col-md-3 control-label">Description</label>
            <div class="col-md-8 col-md-offset-1">
                <textarea type="text" class="form-control input-md">${quiz.description}</textarea>
            </div>
        </div>

        <div class="text-center">
            <button type="button" class="btn btn-primary" id="StartQuizButton">Start Quiz</button>
        </div>
    </div>
</c:if>

<c:if test="${quiz==null}">
    <div class="col-sm-6 col-sm-offset-3 text-center margin_bottom">
        <h4 class="text-danger">Quiz have no questions or does not exists</h4>
    </div>
</c:if>

<div class="col-sm-6 col-sm-offset-3 panel panel-default remove_padding hide start">

    <div class="panel-heading overfl">
        <div class="col-sm-2">
            <label id="questionNo">1</label>
        </div>

        <div class="col-sm-8 text-center">
            <label>${quiz.name_test}</label>
        </div>

        <div class="col-sm-2 text-right">
            <label id="correctNo">1/30</label>
        </div>
    </div>

    <div class="panel-body">
        <form id="Answers" class="form-horizontal" role="form">
            <input id="question_id" name="question_id" class="hide question_id"/>
            <div class="overfl margin_bottom text-left">
                <label class="col-md-12 font_20" id="question">Question</label>  
            </div>

            <div class="form-group text-left">
                <div class="col-sm-1 col-sm-offset-1">
                    <input type="radio" class="ansId" name="correct" value="0">
                </div>
                <div class="col-sm-6">
                    <label id="answer1">Enter Answer</label> 
                </div>
            </div>

            <div class="form-group text-left">
                <div class="col-sm-1 col-sm-offset-1">
                    <input type="radio" class="ansId" name="correct" value="1">
                </div>
                <div class="col-sm-6">
                    <label id="answer2">Enter Answer</label> 
                </div>
            </div>

            <div class="form-group text-left">
                <div class="col-sm-1 col-sm-offset-1">
                    <input type="radio" class="ansId" name="correct" value="2">
                </div>
                <div class="col-sm-6">
                    <label id="answer3">Enter Answer</label> 
                </div>
            </div>

            <div class="form-group text-left">
                <div class="col-sm-1 col-sm-offset-1">
                    <input type="radio" class="ansId" name="correct" value="3">
                </div>
                <div class="col-sm-6">
                    <label id="answer4">Enter Answer</label> 
                </div>
            </div>

        </form>
    </div>

    <div class="panel-footer text-center">
        <button type="button" class="btn btn-primary" id="CheckQuestionButton">Answer</button>
    </div>
</div>

<div id="results" class="col-sm-6 col-sm-offset-3 text-center margin_bottom" style="display:none">
    <div class="form-group margin_bottom overfl">
        <label class="col-md-3 control-label">Correct</label>
        <div class="col-md-8 col-md-offset-1">
            <label class="col-md-3" id="correctAns"></label>
        </div>
    </div>

    <div class="form-group margin_bottom overfl">
        <label class="col-md-3 control-label">Total</label>
        <div class="col-md-8 col-md-offset-1">
            <label class="col-md-3" id="totalNo">Total number</label>
        </div>
    </div>

    <div class="form-group margin_bottom overfl">
        <label class="col-md-3 control-label">Percentage</label>
        <div class="col-md-8 col-md-offset-1">
            <label class="col-md-3" id="percent">Percentage</label>
        </div>
    </div>

    <div class="text-center">
        <button type="button" class="btn btn-primary" id="PreviewAnsButton">Preview</button>
        <button type="button" class="btn btn-primary" id="ReStartQuizButton">Restart</button>
    </div>
</div>