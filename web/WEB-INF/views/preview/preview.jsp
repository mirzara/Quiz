<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="page-header text-center">
    <h3>Preview Answers</h3>
</div>

<c:forEach var="question" items="${questions}" varStatus="loop">
    <div class="col-sm-6 col-sm-offset-3 panel panel-default remove_padding">
        <div class="panel-heading">
            <label class="font_20">${loop.index+1}. ${question.question}</label>
        </div>
        <div class="panel-body">Your answer:
            <c:forEach var="answer" items="${question.answerList}">
                ${answer.answer}<div
                    <c:if test="${answer.correct}">class='text-success'>Correct answer</c:if>
                    <c:if test="${!answer.correct}">class='text-danger'>Wrong answer</c:if>
                    </div>
            </c:forEach>
        </div>
    </div>
</c:forEach>


