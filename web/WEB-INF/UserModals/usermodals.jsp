<!-- Modal za logout -->
<div class="modal fade" id="logout" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="center">Logout</h3>
            </div>
            <div class="modal-body text-center">
                <p>Are you sure you want to logout?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="logoutButton">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal view profile -->
<div id="viewProfile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="center">View Profile</h3>
            </div>
            <div class="modal-body">
                <form id="viewProfileForm" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-md-3 control-label">First name*</label>  
                        <div class="col-md-8">
                            <input id="fname" name="fname" type="text" maxlength="50" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Last name*</label>  
                        <div class="col-md-8">
                            <input id="lname" name="lname" type="text" maxlength="50" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Phone</label>  
                        <div class="col-md-8">
                            <input id="phone" name="phone" type="text" maxlength="30" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Address</label>  
                        <div class="col-md-8">
                            <input id="address" name="address" type="text" maxlength="50" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Email*</label>  
                        <div class="col-md-8">
                            <input id="email" name="email" type="text" maxlength="50" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group response">
                        <div class="col-sm-12">
                            <div class="alert alert-danger">

                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="SaveProfileButton">Save changes</button>
                <button type="button" class="btn btn-danger" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#deleteAcc">Delete Account</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal delete -->
<div class="modal fade" id="deleteAcc" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="center">Delete Account</h3>
            </div>
            <div class="modal-body overfl text-center">

                <p>Your account is going to be deleted</p>
                <div class="form-group response">
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            Cant delete account
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="DeleteAccButton">Proceed</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Change password -->
<div class="modal fade" id="ChangePass" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="center">Change password</h3>
            </div>
            <div class="modal-body">

                <form id="changePassForm" class="form-horizontal" role="form">

                    <div class="form-group">
                        <label class="col-md-3 control-label">Old*</label>  
                        <div class="col-md-8">
                            <input id="oldPass" name="oldPass" type="password" maxlength="50" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">New*</label>  
                        <div class="col-md-8">
                            <input id="newPass" name="newPass" type="password" maxlength="50" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group response">
                        <div class="col-sm-12">
                            <div class="alert alert-danger">

                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="changePassButton">Change</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>