CREATE USER 'java'@'localhost' IDENTIFIED BY 'mypass';
 grant all privileges on `quizDb`.* to 'java'@'localhost' identified by 'mypass';

CREATE DATABASE IF NOT EXISTS quizDb;

USE quizDb;

CREATE TABLE IF NOT EXISTS users(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	address varchar(50),
	phone varchar(30),
	email varchar(50) NOT NULL,
	password varchar(50) NOT NULL,
	register_date DATETIME NOT NULL,
	last_login DATETIME
);

CREATE TABLE IF NOT EXISTS category(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name varchar(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS test(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name_test varchar(50) NOT NULL,
	description varchar(255) NOT NULL,
	id_users INT NOT NULL,
	id_category INT NOT NULL,
	FOREIGN KEY (id_users) REFERENCES users(id),
	FOREIGN KEY (id_category) REFERENCES category(id)
);

CREATE TABLE IF NOT EXISTS question(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	question varchar(255) NOT NULL,
	id_test INT NOT NULL,
	FOREIGN KEY (id_test) REFERENCES test(id)
);

CREATE TABLE IF NOT EXISTS answer(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	answer varchar(50) NOT NULL,
	correct TINYINT(1) NOT NULL,
	id_question INT NOT NULL,
	FOREIGN KEY (id_question) REFERENCES question(id)
);

CREATE TABLE IF NOT EXISTS results(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	from_percentage INT (3) NOT NULL,
	till_percentage INT (3) NOT NULL,
	title varchar(50) NOT NULL,
	description varchar(255),
	id_test INT NOT NULL,
	FOREIGN KEY (id_test) REFERENCES test(id)
);

INSERT INTO quizDb.category VALUES(null, 'Books');
INSERT INTO quizDb.category VALUES(null, 'Celebrities');
INSERT INTO quizDb.category VALUES(null, 'Film');
INSERT INTO quizDb.category VALUES(null, 'Sport');
INSERT INTO quizDb.category VALUES(null, 'Music');
INSERT INTO quizDb.category VALUES(null, 'Computers');
INSERT INTO quizDb.category VALUES(null, 'Other');
