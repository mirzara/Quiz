## Quiz

Quiz is a simple web application made with Java in a backend. It enables standard crud operation for quizzes. Only registered users can manage their own quizzes while anyone else can see and run them.

Quiz consists of question and answers. It must belong to its category for easier preview or searching. At the end of running quiz percentage and number of correctly answered questions are showed. There is also a functionality to preview all of your answered questions.

It uses Servlets, JSP and JPA/Persistence. Application was tested on Glassfish 4.1 and MySQL database. For initial display of data from database JSP is used, while for others operations such as crud requests are sent with AJAX.

Crud operations are limited to only registered users, which is implemented with Servlet filter.
Supported features are:

## User management
* Create or delete account
* View or update profile
* Change password

## Session management
* Login
* Logout

## Quiz management
* Crud operations
* Add edit or remove of questions and answers
* Add edit or remove of results
* Search by name, description under all or specific category
* Sort by name, description or category
* Run quiz

GUI is built with [Twitter Bootstrap](http://getbootstrap.com/), HTML, CSS, JS and jQuery.

## Build and run Quiz project
* Clone the repository git clone https://gitlab.com/mirzara/quiz.git
* Run quizDb SQL script to set up a database (MySQL)
* Open the project with [Netbeans](https://netbeans.org/), build and run
* Open the link in your browser: http://localhost:8080/Quiz
* Rest is up to you

## TODO
- [x] Random question order
- [x] Improve layout
- [x] Show all results related to quiz at the end of quiz
- [x] Add SQL scripts


## License 
Quiz is released under MIT license
