/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Preview;

import Servlet.common.quizComm;
import entity.Answer;
import entity.Question;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class previewAnswersServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session=request.getSession();
        
        List<String> preview = (List<String>) session.getAttribute("previewAns");
        
        List<Question> questionList = (List<Question>) session.getAttribute("questions");
        List<Question> newQuestions = new ArrayList();

        if (preview == null || preview.isEmpty()) {
            return;
        }

        if (preview.size() != questionList.size()) {
            return;
        }
        
        for (int i = 0; i < preview.size(); i++) {
            String a = preview.get(i);
            
            Answer ans = new Answer();
            if (quizComm.isNumber(a)) {
                int no = Integer.parseInt(a);
                ans = quizComm.findInList(no, questionList.get(i).getAnswerList());
            }
            
            List<Answer> answerSend = new ArrayList();
            answerSend.add(ans);

            Question questionSend = questionList.get(i);
            questionSend.setAnswerList(answerSend);
            newQuestions.add(questionSend);
        }

        request.setAttribute("questions", newQuestions);
        request.getRequestDispatcher("WEB-INF/views/preview/preview.jsp").forward(request, response);
    }
}
