/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.Home;

import Servlet.common.quizComm;
import dao.CategoryDaoLocal;
import dao.QuizDaoLocal;
import entity.Quiz;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class HomeServlet extends HttpServlet {

    @EJB
    private CategoryDaoLocal category;

    @EJB
    private QuizDaoLocal quiz;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("user");
        String sortBy = sorting(request.getParameter("sort"));
        String search = search(request.getParameter("search"), sortBy);
        String categoryId = request.getParameter("category");

        if (request.getServletPath().equals("/Quizzes")) {
            request.setAttribute("show", false);
            filterBy(2, id, categoryId, sortBy, search, request);
        } else {
            request.setAttribute("show", true);
            request.setAttribute("fullName", session.getAttribute("user_fullname"));
            filterBy(1, id, categoryId, sortBy, search, request);
        }
        
        request.setAttribute("category", category.getAll());
        request.getRequestDispatcher("WEB-INF/views/home/home.jsp").forward(request, response);
    }

    public String sorting(String sortBy) {
        String sorting = "id";

        if (sortBy == null) {
            return sorting;
        }

        if (sortBy.replaceAll(" ", "").isEmpty()) {
            return sorting;
        }

        switch (sortBy) {

            case "1":
                sorting = "name_test";
                break;
            case "2":
                sorting = "description";
                break;

        }

        return sorting;
    }

    public String search(String search, String sort) {
        if (search == null) {
            return "";
        }

        if (search.replaceAll(" ", "").isEmpty()) {
            return "";
        }

        if (sort.equals("id")) {
            return "name_test LIKE '%" + search + "%'";
        }

        search = sort + " LIKE '%" + search + "%'";
        return search;
    }

    public void filterBy(int num, Integer id, String categoryId, String sortBy, String search,
            HttpServletRequest request) {

        List<Quiz> quizL;

        if (quizComm.isNumber(categoryId)) {
            quizL = CategoryAllorFromUser(num, id, Integer.parseInt(categoryId), sortBy, search);
        } else {
            quizL = QuizAllorUser(num, id, sortBy, search);
        }

        request.setAttribute("quiz", quizL);
    }

    public List<Quiz> CategoryAllorFromUser(int num, Integer id, int categoryId, String sortBy, String search) {

        if (num == 1 && id != null) {
            return quiz.getAllByCatFromUser(id, categoryId, sortBy, search);
        } else if (num == 2) {
            return quiz.getAllByCat(categoryId, sortBy, search);
        }
        return null;
    }

    public List<Quiz> QuizAllorUser(int num, Integer id, String sortBy, String search) {
        
        if (num == 1 && id != null) {
            return quiz.getAllFromUser(id, sortBy, search);
        } else if (num == 2) {
            return quiz.getAll(sortBy, search);
        }

        return null;
    }
}
