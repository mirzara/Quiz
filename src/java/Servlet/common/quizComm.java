/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.common;

import entity.Answer;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class quizComm {

    public static Answer findInList(int id, List<Answer> answerList) {

        for (Answer ans1 : answerList) {
            if (ans1.getId().equals(id)) {
                return ans1;
            }
        }
        return null;
    }

    public static boolean isNumber(String number) {
        if (number == null) {
            return false;
        }

        if (number.replaceAll(" ", "").isEmpty()) {
            return false;
        }

        for (int i = 0; i < number.length(); i++) {
            int a = number.charAt(i);

            if (!(a >= 48 && a <= 58)) {
                return false;
            }
        }

        return true;
    }

    public static boolean checkRadio(int checked, int a) {

        if (checked == a) {
            return true;
        }

        return false;
    }

    public static boolean checkFields(List<String> lista) {

        for (int i = 0; i < lista.size(); i++) {
            String value = lista.get(i);

            if (value == null) {
                return false;
            }

            if (value.replaceAll(" ", "").isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public static List<String> fillList(HttpServletRequest request) {
        List<String> list = new ArrayList();

        for (int i = 0; i < 4; i++) {
            String answerField = request.getParameter("answer" + (i + 1));
            if (answerField.replaceAll(" ", "").isEmpty()) {
                return null;
            }

            list.add(answerField);
        }
        return list;
    }
}
