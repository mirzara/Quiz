/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.User;

import Servlet.common.userComm;
import dao.UserDaoLocal;
import entity.User;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class saveProfileServlet extends HttpServlet {

    @EJB
    private UserDaoLocal user;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String email = request.getParameter("email");

        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("user");

        boolean check = userComm.checkRequiredFields(email, fname, lname, null);
        if (check) {
            response.getWriter().write("E1");
            return;
        }

        User update = user.getById(id);
        boolean mailChecks = checkAndValidate(update, email);

        if (!mailChecks) {
            response.getWriter().write("E2");
            return;
        }

        update.setFirst_name(fname);
        update.setLast_name(lname);
        update.setPhone(phone);
        update.setAddress(address);
        update.setEmail(email);

        user.updateUser(update);

        session.setAttribute("user_fullname", fname + " " + lname);
        response.getWriter().write("S");
    }

    public boolean checkAndValidate(User update, String email) {
        User checkMail = user.getByEmail(email);

        if (!userComm.validateEmail(email)) {
            return false;
        }

        if (checkMail != null) {
 
            if (!checkMail.getEmail().equals(update.getEmail())) {
                return false;
            }
        }

        return true;
    }
}
