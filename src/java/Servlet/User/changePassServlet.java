/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.User;

import Servlet.common.quizComm;
import dao.UserDaoLocal;
import entity.User;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class changePassServlet extends HttpServlet {

    @EJB
    private UserDaoLocal user;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String oldPass = request.getParameter("oldPass");
        String newPass = request.getParameter("newPass");

        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("user");

        User dbUser = user.getById(id);

        List<String> lista = Arrays.asList(oldPass, newPass);
        
        if (!quizComm.checkFields(lista)) {
            response.getWriter().write("E1");
            return;
        }
       
        if (newPass.length() < 6) {
            response.getWriter().write("E4");
            return;
        }
        if (dbUser == null) {
            response.getWriter().write("E3");
            return;
        }

        if (!dbUser.getPassword().equals(oldPass)) {
            response.getWriter().write("E5");
            return;
        }

        dbUser.setPassword(newPass);
        boolean changed = user.updateUser(dbUser);
        if (!changed) {
            response.getWriter().write("E5");
            return;
        }

        response.getWriter().write("S");
    }
}
