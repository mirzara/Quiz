/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.User;

import dao.UserDaoLocal;
import entity.User;
import java.io.IOException;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class viewProfileServlet extends HttpServlet {

    @EJB
    private UserDaoLocal user;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session=request.getSession();
        Integer id=(Integer)session.getAttribute("user");
        
        if(id==null)
            return;
        
        User db=user.getById(id);
        response.setContentType("application/json; charset=UTF-8");
        response.getWriter().print(form_json(db));
    }
    
    public JsonObject form_json(User db)
    {
        JsonObject json=Json.createObjectBuilder()
                .add("fname", (db.getFirst_name()==null)?"":db.getFirst_name())
                .add("lname", (db.getLast_name()==null)?"":db.getLast_name())
                .add("phone", (db.getPhone()==null)?"":db.getPhone())
                .add("address",(db.getAddress()==null)?"":db.getAddress())
                .add("email", (db.getEmail()==null)?"":db.getEmail())
                .build();
        
        return json;
    }
}
