/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.Quiz.Results;

import Servlet.common.quizComm;
import dao.ResultsDaoLocal;
import entity.Quiz;
import entity.Results;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class saveResultServlet extends HttpServlet {

    @EJB
    private ResultsDaoLocal result;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String quiz_id = request.getParameter("quiz_id");
        String action = request.getParameter("action");
        String from_p = request.getParameter("from_p");
        String till_p = request.getParameter("till_p");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        
        List<String> lista=Arrays.asList(from_p,till_p,title);
        
        if(!quizComm.checkFields(lista)){
            response.getWriter().write("E1");
            return;
        }

        Integer fromp = Integer.parseInt(from_p);
        Integer tillp = Integer.parseInt(till_p);
        Integer QuizId = Integer.parseInt(quiz_id);
        
        if (action.equals("add")) {
            
            Quiz quizDb = new Quiz();
            quizDb.setId(QuizId);
            Results saveResult = new Results(quizDb, fromp, tillp, title, description);
            result.addResult(saveResult);
            
        } else if (action.equals("edit")) {
            
            Integer res_Id = Integer.parseInt(id);
            EditResults(res_Id, fromp, tillp, title, description);
        }
        
        response.getWriter().write("S");
    }
    
    public void EditResults(int id, int fromp, int tillp, String title, String desc){
        Results Update_res=result.getById(id);
        
        Update_res.setFrom_percentage(fromp);
        Update_res.setTill_percentage(tillp);
        Update_res.setTitle(title);
        Update_res.setDescription(desc);
        
        result.updateResult(Update_res);
    }
}
