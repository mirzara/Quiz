/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.Quiz;

import Servlet.common.quizComm;
import dao.QuestionDaoLocal;
import entity.Answer;
import entity.Question;
import entity.Results;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class runQuizServlet extends HttpServlet {

    @EJB
    private QuestionDaoLocal question;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        HttpSession session = request.getSession();
        Integer idNo = Integer.parseInt(id);

        List<Question> questionList = question.getByQuiz(idNo);
        session.setAttribute("questions", questionList);

        removeSessionAttributes(session);
        request.setAttribute("quiz", null);

        if (questionList.size() > 0) {
            request.setAttribute("quiz", questionList.get(0).getId_test());
        }

        request.getRequestDispatcher("WEB-INF/views/quiz/run.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String correctAnswer = request.getParameter("correct");
        HttpSession session = request.getSession();
        List<Question> questionList1 = (List<Question>) session.getAttribute("questions");
        Integer number = (Integer) session.getAttribute("number");
        response.setContentType("application/json; charset=UTF-8");

        int total = questionList1.size();
        int questionNo = 0;
        Question questionPrev = questionList1.get(questionNo);

        if (number != null) {
            questionPrev = questionList1.get(number);
            questionNo = number + 1;
        }

        boolean correct = checkCorrect(correctAnswer, questionPrev.getAnswerList());
        addPreview(correctAnswer, number, session);
        updateCorrectWrong(session, correct);
        Integer correctNo = (Integer) session.getAttribute("correct");

        if (questionNo >= questionList1.size()) {
            List<Results> resList = questionList1.get(0).getId_test().getResultsList();
            response.getWriter().print(obj(null, correctNo, true, total, questionNo, resList));
            return;
        }

        session.setAttribute("number", questionNo);
        Question question1 = questionList1.get(questionNo);
        response.getWriter().print(obj(question1, correctNo, false, total, questionNo, null));
    }

    public JsonObject obj(Question question, int correctNo, boolean end,
            int total, int qNo, List<Results> resList) {

        JsonObjectBuilder obj2 = Json.createObjectBuilder();

        obj2.add("success", true);
        obj2.add("correct", correctNo);
        obj2.add("total", total);
        obj2.add("end", end);

        if (end) {
            obj2.add("results", resultsObj(resList));
        } else if (question != null) {
            obj2.add("qNo", qNo + 1);
            obj2.add("question", question.getQuestion());
            obj2.add("answers", answersObj(question));
        }

        return obj2.build();
    }

    public JsonArrayBuilder answersObj(Question question) {

        List<Answer> answerList = question.getAnswerList();
        JsonObjectBuilder answers = Json.createObjectBuilder();

        JsonArrayBuilder ansArray = Json.createArrayBuilder();
        for (int i = 0; i < answerList.size(); i++) {

            answers.add("id", answerList.get(i).getId());
            answers.add("answer", answerList.get(i).getAnswer());
            ansArray.add(answers);

        }

        return ansArray;
    }

    public JsonArrayBuilder resultsObj(List<Results> resList) {

        JsonObjectBuilder allRes = Json.createObjectBuilder();

        JsonArrayBuilder ResArray = Json.createArrayBuilder();
        for (int i = 0; i < resList.size(); i++) {

            allRes.add("from_p", resList.get(i).getFrom_percentage());
            allRes.add("till_p", resList.get(i).getTill_percentage());
            allRes.add("title", resList.get(i).getTitle());
            allRes.add("description", resList.get(i).getDescription());
            ResArray.add(allRes);

        }

        return ResArray;
    }

    public boolean checkCorrect(String correctAnswer, List<Answer> answerList) {

        if (correctAnswer == null) {
            return false;
        }

        if (!quizComm.isNumber(correctAnswer)) {
            return false;
        }

        Integer id = Integer.parseInt(correctAnswer);
        Answer ans1 = quizComm.findInList(id, answerList);

        if (ans1 != null) {
            return ans1.isCorrect();
        }

        return false;
    }

    public void updateCorrectWrong(HttpSession session, boolean correct) {
        Integer correctAnswer = (Integer) session.getAttribute("correct");
        int correctNumber = 0;

        if (correctAnswer != null) {
            correctNumber = correctAnswer;
        }
        if (correct) {
            correctNumber = correctAnswer + 1;
        }

        session.setAttribute("correct", correctNumber);
    }

    public void removeSessionAttributes(HttpSession session) {

        session.removeAttribute("correct");
        session.removeAttribute("number");
        session.removeAttribute("previewAns");

    }
    
    public void addPreview(String correctAns, Integer number, HttpSession session){
        if(number==null)
            return;
        
        List<String> preview = (List<String>) session.getAttribute("previewAns");
        
        if(preview==null || preview.isEmpty())
            preview=new ArrayList();
        
        preview.add(correctAns);
        session.setAttribute("previewAns",preview);
    }
}
