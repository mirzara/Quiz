/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.Quiz.QuestionAnswer;

import Servlet.common.quizComm;
import dao.QuestionDaoLocal;
import entity.Answer;
import entity.Question;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class editQAServlet extends HttpServlet {

    @EJB
    private QuestionDaoLocal questionDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Integer id_user = (Integer) session.getAttribute("user");
        String action = request.getParameter("action");
        String questionId = request.getParameter("question_id");
        String question = request.getParameter("question");
        String radioVal = request.getParameter("correct");
        List<String> answerReq = quizComm.fillList(request);

        List<String> lista = Arrays.asList(action, questionId, question, radioVal);

        if (!quizComm.checkFields(lista) || answerReq == null) {
            response.getWriter().write("E1");
            return;
        }

        Question EditQ = questionDao.getById(Integer.parseInt(questionId));

        if (EditQ == null) {
            response.getWriter().write("E9");
            return;
        }

        if (!EditQ.getId_test().getId_users().getId().equals(id_user)) {
            response.getWriter().write("E8");
            return;
        }

        if (action.equals("delete")) {
            questionDao.deleteQuestion(EditQ);
            response.getWriter().write("D");
            return;
        }
        
        EditQ.setQuestion(question);
        List<Answer> answerList = EditQ.getAnswerList();

        for (int i = 0; i < answerList.size(); i++) {
            answerList.get(i).setCorrect(false);
            answerList.get(i).setAnswer(answerReq.get(i));

            if (quizComm.checkRadio(Integer.parseInt(radioVal), i)) {
                answerList.get(i).setCorrect(true);
            }
        }

        EditQ.setAnswerList(answerList);
        questionDao.updateQuestion(EditQ);
        response.getWriter().write("S");
    }
}
