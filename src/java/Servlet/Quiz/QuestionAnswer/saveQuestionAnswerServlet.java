/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.Quiz.QuestionAnswer;

import Servlet.common.quizComm;
import dao.AnswerDaoLocal;
import dao.QuestionDaoLocal;
import dao.QuizDaoLocal;
import entity.Answer;
import entity.Question;
import entity.Quiz;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class saveQuestionAnswerServlet extends HttpServlet {

    @EJB
    private QuizDaoLocal quizDao;

    @EJB
    private QuestionDaoLocal questionDao;

    @EJB
    private AnswerDaoLocal answerDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Integer id_user = (Integer) session.getAttribute("user");
        String quiz_id = request.getParameter("quiz_id");
        String question = request.getParameter("question");
        String radioVal = request.getParameter("correct");

        List<String> answerList = quizComm.fillList(request);
        Quiz quizBelong = quizDao.getById(Integer.parseInt(quiz_id));

        if (quizBelong != null) {
            if (!quizBelong.getId_users().getId().equals(id_user)) {
                response.getWriter().write("E8");
                return;
            }
        }

        List<String> lista = Arrays.asList(radioVal, quiz_id, question);

        if (!quizComm.checkFields(lista) || answerList == null) {
            response.getWriter().write("E1");
            return;
        }
        
        Integer checked = Integer.parseInt(radioVal);

        Quiz newQuiz = new Quiz();
        newQuiz.setId(Integer.parseInt(quiz_id));
        Question questionDb = new Question(question, newQuiz);

        Question mal = questionDao.addQuestion(questionDb);
        for (int i = 0; i < 4; i++) {
            String txt = answerList.get(i);
            boolean correctAns = quizComm.checkRadio(checked, i);
            Answer ans = new Answer(txt, mal, correctAns);
            answerDao.addAnswer(ans);
        }
        response.getWriter().write("S");
    }
}
