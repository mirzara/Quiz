/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Servlet.Quiz;

import Servlet.common.quizComm;
import dao.CategoryDaoLocal;
import dao.QuizDaoLocal;
import entity.Category;
import entity.Question;
import entity.Quiz;
import entity.User;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
public class createQuizServlet extends HttpServlet {

    @EJB
    private QuizDaoLocal quiz;

    @EJB
    private CategoryDaoLocal category;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id_quiz = request.getParameter("id");
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("user");

        id_quiz = (id_quiz == null) ? "" : id_quiz;
        if (id_quiz.replaceAll(" ", "").isEmpty()) {
            response.getWriter().write("Quiz not found");
            return;
        }

        Quiz db = quiz.getById(Integer.parseInt(id_quiz));

        if (db != null) {
            if (!db.getId_users().getId().equals(id)) {
                response.getWriter().write("You cant change quiz that doesnt belong to you");
                return;
            }
        } else {
            response.getWriter().write("No such Quiz");
            return;
        }

        String doWhat = request.getServletPath();

        if (doWhat.equals("/createQuiz")) {

            request.setAttribute("id", id_quiz);
            request.getRequestDispatcher("WEB-INF/views/quiz/create.jsp").forward(request, response);

        } else if (doWhat.equals("/editQuiz")) {

            List<Question> questions = db.getQuestionList();

            request.setAttribute("quiz", db);
            request.setAttribute("questions", questions);
            request.setAttribute("category", category.getAll());
            request.setAttribute("result", db.getResultsList());
            request.getRequestDispatcher("WEB-INF/views/quiz/edit.jsp").forward(request, response);

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String quiz_name = request.getParameter("quiz_name");
        String description = request.getParameter("description");
        String categoryId = request.getParameter("categ");
        String id = request.getParameter("id");
        String action = request.getParameter("action");

        HttpSession session = request.getSession();
        Integer id_user = (Integer) session.getAttribute("user");

        response.setContentType("application/json; charset=UTF-8");

        List<String> lista = Arrays.asList(quiz_name, description, categoryId);

        if (!quizComm.checkFields(lista)) {
            response.getWriter().print(jsonFalse("E1"));
            return;
        }
        
        if (action.equals("edit")) {
            Integer catId = Integer.parseInt(categoryId);
            Quiz quizy = quiz.getById(Integer.parseInt(id));
            boolean edited = EditQuiz(quiz_name, description, quizy, id_user, catId);
            if (!edited) {
                response.getWriter().print(jsonFalse("E7"));
                return;
            }
            response.getWriter().print(jsonResult(quizy));
            return;
        }

        User user = new User();
        user.setId(id_user);

        Category cat = new Category();
        cat.setId(Integer.parseInt(categoryId));

        Quiz saveQuiz = new Quiz(quiz_name, description, user, cat);
        Quiz dbQuiz = quiz.createQuiz(saveQuiz);

        response.getWriter().print(jsonResult(dbQuiz));
    }

    public boolean EditQuiz(String quiz_name, String description, Quiz quizy, int id_user,
            int category) {
        if (id_user != quizy.getId_users().getId()) {
            return false;
        }

        Category cat = new Category();
        cat.setId(category);

        quizy.setName_test(quiz_name);
        quizy.setDescription(description);
        quizy.setCategory(cat);

        quiz.updateQuiz(quizy);
        return true;
    }

    public JsonObject jsonResult(Quiz obj) {

        JsonObject retJson = Json.createObjectBuilder()
                .add("success", true)
                .add("id", obj.getId())
                .build();

        return retJson;
    }

    public JsonObject jsonFalse(String err) {

        JsonObject retJson = Json.createObjectBuilder()
                .add("success", false)
                .add("message", err)
                .build();

        return retJson;
    }
}
