/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dao;

import entity.Question;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
@Stateless
public class QuestionDao implements QuestionDaoLocal {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Question getById(int id) {
        return manager.find(Question.class, id);
    }

    @Override
    public void updateQuestion(Question question) {
        manager.merge(question);
    }

    @Override
    public void deleteQuestion(Question question) {
        Question remove = manager.merge(question);
        manager.remove(remove);
    }

    @Override
    public List<Question> getByQuiz(int id) {
        List<Question> question = manager.createNativeQuery("select * from question where id_test=? order by rand()", Question.class)
                .setParameter(1, id)
                .getResultList();

        return question;
    }

    @Override
    public void deleteByQuiz(int id) {
        manager.createNativeQuery("delete from question where id_test=?")
                .setParameter(1, id)
                .executeUpdate();

    }

    @Override
    public Question addQuestion(Question question) {
        manager.persist(question);
        manager.flush();
        return question;
    }
}
