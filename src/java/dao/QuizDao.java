/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dao;

import entity.Quiz;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
@Stateless
public class QuizDao implements QuizDaoLocal {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Quiz createQuiz(Quiz quiz) {
        manager.persist(quiz);
        manager.flush();
        manager.refresh(quiz);
        return quiz;
    }

    @Override
    public void updateQuiz(Quiz quiz) {
        manager.merge(quiz);
    }

    public String form_Query(String input, String sort, String search) {

        String query = "select * from test where " + input;

        if (!search.equals("")) {
            query = query + " and " + search;
        }

        query = query + " order by " + sort;

        return query;
    }

    @Override
    public List<Quiz> getAllByCat(int categoryId, String sort, String search) {

        String Query = form_Query("id_category=?", sort, search);

        List<Quiz> quizes = manager.createNativeQuery(Query, Quiz.class)
                .setParameter(1, categoryId)
                .getResultList();

        return quizes;
    }

    @Override
    public List<Quiz> getAllByCatFromUser(int id, int categoryId, String sort, String search) {

        String Query = form_Query("id_users=? and id_category=?", sort, search);

        List<Quiz> quizes = manager.createNativeQuery(Query, Quiz.class)
                .setParameter(1, id)
                .setParameter(2, categoryId)
                .getResultList();

        return quizes;
    }

    @Override
    public List<Quiz> getAllFromUser(int id, String sort, String search) {

        String sortQuery = form_Query("id_users=?", sort, search);

        List<Quiz> quiz = manager.createNativeQuery(sortQuery, Quiz.class)
                .setParameter(1, id)
                .getResultList();

        return quiz;
    }

    @Override
    public List<Quiz> getAll(String sort, String search) {
        String sortQuery = "select * from test order by " + sort;

        List<Quiz> quiz = manager.createNativeQuery(sortQuery, Quiz.class)
                .getResultList();

        return quiz;
    }

    @Override
    public Quiz getById(int id) {
        return manager.find(Quiz.class, id);
    }

    @Override
    public void removeQuiz(Quiz quiz) {
        Quiz remove = manager.merge(quiz);
        manager.remove(remove);
    }
}
