/*
 * The MIT License
 *
 * Copyright 2016 Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dao;

import entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Mirza Rahmanović <Mirza.Rahmanovic@outlook.com>
 */
@Stateless
public class UserDao implements UserDaoLocal {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public boolean addUser(User user) {
        manager.persist(user);
        return true;
    }

    @Override
    public boolean updateUser(User user) {
        manager.merge(user);
        return true;
    }

    @Override
    public boolean removeUser(int id) {
        User removeUser=manager.find(User.class,id);
        manager.remove(removeUser);
        return true;
    }

    @Override
    public User checkUser(String email, String pass) {
        List<User> user = manager.createNativeQuery("select * from users where email=? and password=?", User.class)
                .setParameter(1, email)
                .setParameter(2, pass)
                .getResultList();

        if (user.size() > 0) {
            return user.get(0);
        } else {
            return null;
        }
    }

    @Override
    public User getByEmail(String email) {
        List<User> user = manager.createNativeQuery("select * from users where email=?", User.class)
                .setParameter(1, email)
                .getResultList();

        if (user.size() > 0) {
            return user.get(0);
        } else {
            return null;
        }
    }

    @Override
    public User getById(int id) {
        return manager.find(User.class, id);
    }
}
